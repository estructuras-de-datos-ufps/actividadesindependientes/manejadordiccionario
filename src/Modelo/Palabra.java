/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import util.ufps.colecciones_seed.VectorGenerico;

/**
 *
 * @author madar
 */
public class Palabra {
    
    private VectorGenerico<Character> caracteres;

    public Palabra() {
    }

    /**
     * A partir de la cadena crea el vector genérico
     *  Ejemplo: Cadena="ufps", this.caracteres={"u","f","p","s"}
     * @param cadena un String a ser pasado al vector
     */
    public Palabra(String cadena)
    {
    
    }
    
    public VectorGenerico<Character> getCaracteres() {
        return caracteres;
    }

    public void setCaracteres(VectorGenerico<Character> caracteres) {
        this.caracteres = caracteres;
    }
    
    
}
